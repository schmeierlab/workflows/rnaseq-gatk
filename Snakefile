import csv, sys
from os.path import join, abspath

# Snakefile config file goes here
configfile: "config.json"

REFDIR = abspath(config["REFDIR"])
GENOME = config["GENOME"]
FAIDX = config["FAIDX"]
DICTIONARY = config["DICTIONARY"]
DBSNP = config["DBSNP"]
SNPIDX = config["SNPIDX"]
COSMIC = config["COSMIC"]
COSMICIDX = config["COSMICIDX"]
GNOMAD = config["GNOMAD"]
GNOMADIDX = config["GNOMADIDX"]
CLINVAR1 = config["CLINVAR1"]
CLINVAR1IDX = config["CLINVAR1IDX"]
CLINVAR2 = config["CLINVAR2"]
CLINVAR2IDX = config["CLINVAR2IDX"]
ExAC = config["ExAC"]
ExACIDX = config ["ExAC"]
GTF = config["GTF"]
GENOMEIDXDIR = abspath(config["GENOMEIDXDIR"])
SJDBOVERHANG = config["SJDBOVERHANG"]
SAMPLEDIR = abspath(config["SAMPLEDIR"])
SAMPLESHEET = abspath(config["SAMPLESHEET"])
SCRIPTS = abspath(config["SCRIPTS"])
RESULTS = abspath(config["RESULTS"])
MULTIQC_COL = config["MULTIQC_COL"]

BAMDIR = join(RESULTS, "bam")
STARONE = join(BAMDIR, "Star1Pass")
STARTWO  = join(BAMDIR, "Star2Pass")
PREDIR = join(RESULTS, "preprocessing")
VARDIR = join(RESULTS, "variants")
FILDIR = join(RESULTS, "filtered_variants")
STATDIR = join(RESULTS, "stats")
LOGDIR = join(RESULTS, "logs")
BENCHDIR = join(RESULTS, "benchmarks")
REPORTS = join(RESULTS, "reports")
#SAMPLES, = glob_wildcards(join(SAMPLEDIR, "{SAMPLE}_1.fastq.gz"))

#===================================
# reading samplename from samplesheet
sys.stderr.write("Reading samples from samplesheet...\n")
SAMPLES = []
reader = csv.reader(open(SAMPLESHEET), delimiter="\t")
for a in reader:
    SAMPLES.append(a[0])

# test if sample in dir
for fname in expand(join(SAMPLEDIR, "{sample}_1.fastq.gz"), sample=SAMPLES):
    if not os.path.isfile(fname):
        sys.stderr.write("File '{}' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
        sys.exit()

NUM_SAMPLES = len(SAMPLES)
sys.stderr.write('{} samples to process\n'.format(NUM_SAMPLES))
print(','.join(SAMPLES))
#===================================

rule all:
# Variation of expand(join(NEWDIR, "{sample}.new.output"), sample=SAMPLES)
    input:
        join(GENOMEIDXDIR, "INDEX.DONE"),
        join(REFDIR, "dict.done"),
        join(REFDIR, "faidx.done"),
        join(REFDIR, "snpidx.done"),
        join(BAMDIR, "SJ.all.chr.all.samples"),
        expand(join(BAMDIR, "AddReadGroups/{sample}.bam"), sample=SAMPLES),
        expand(join(PREDIR, "Opossum/{sample}.bam"), sample=SAMPLES),
        expand(join(PREDIR, "GATK/04BQSR/{sample}/{sample}.recal.bam"), sample=SAMPLES),
#        expand(join(VARDIR, "GATK/HaplotypeCaller/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "Opossum/HaplotypeCaller/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "GATK/Freebayes/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "Opossum/Freebayes/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "Opossum/Platypus/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "GATK/Mutect2/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "Opossum/Mutect2/{sample}.vcf"), sample=SAMPLES),
#        expand(join(VARDIR, "CombineVariants/{sample}.vcf"), sample=SAMPLES),
        expand(join(VARDIR, "CombineVariants/{sample}.vcf.gz"), sample=SAMPLES),
        expand(join(VARDIR, "CombineVariants/{sample}.vcf.gz.tbi"), sample=SAMPLES),
#        expand(join(FILDIR, "01rawSNP/{sample}.vcf.gz"), sample=SAMPLES),
#        expand(join(FILDIR, "02AddSNPfilters/{sample}.vcf.gz"), sample=SAMPLES),
#        expand(join(FILDIR, "03AnnotateSNP/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "04AnnotateDBSNP/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "05AnnotateCOSMIC/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "06AnnotateGNOMAD/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "07AnnotateCLINVAR1/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "08AnnotateCLINVAR2/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "09AnnotateExAC/{sample}.vcf"), sample=SAMPLES),
#        expand(join(FILDIR, "10ApplySnpFilters/{sample}.vcf"), sample=SAMPLES),
        join(FILDIR, "10ApplySnpFilters/Moderate/ModerateSnpTable.txt"),
        join(FILDIR, "10ApplySnpFilters/High/HighSnpTable.txt"),
        directory(REPORTS),
        join(FILDIR, "10ApplySnpFilters/Moderate/unique_genes.txt"),
        join(FILDIR, "10ApplySnpFilters/High/unique_genes.txt"),
        expand(join(FILDIR, "10ApplySnpFilters/Moderate/{sample}.vcf.gz"), sample=SAMPLES),
        expand(join(FILDIR, "10ApplySnpFilters/High/{sample}.vcf.gz"), sample=SAMPLES),

#### Genome Preparation for Alignment ####
# The following rules prepares the genome for read alignment and variant
# calling. This includes genearating a STAR index, a fasta index, a genome
# dictionary, updating the database of known SNPs (dbSNP) with contigs
# contained within the sequence dictionary (if not already done so), and
# producing a tabix index for the dbSNP.

rule StarIndex:
# Generates genome indexes
# Using STAR 2.6.1d avoids incompatibility issues not (yet) addressed in
# STAR 2.7.1a
    input: 
        genome = GENOME,
        gtf = GTF
    output:
        touch(join(GENOMEIDXDIR, "INDEX.DONE"))
    log:
        join(LOGDIR, "star_indexing.stderr")
    benchmark:
        join(BENCHDIR, "star_indexing.txt")
    conda:
        "envs/star.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads:
        8
    params:
        overhang = SJDBOVERHANG
    shell:
        """
        STAR \
        --runThreadN {threads} \
        --runMode genomeGenerate \
        --genomeDir {GENOMEIDXDIR} \
        --genomeFastaFiles {input.genome} \
        --sjdbGTFfile {input.gtf} \
        --sjdbOverhang {params.overhang} \
        2> {log}
        """

rule GenomeDictionary:
# Create a genome ".dict" file via PICARD
# If a second ".dict" file has been erroneously created beside the one
# intended, it needs to be removed to avoid a conflict with the
# "MergeBamAlignment" rule.
# NOTE: This rule has been known to produce bad ".dict" files, and so
# you may wish to consider hard-coding the command to avoid this issue.
    input:
        GENOME
    output:
        dict = DICTIONARY,
        done = touch(join(REFDIR, "dict.done"))
    log:
        join(LOGDIR, "dict/dictionary.stderr")
    benchmark:
        join(BENCHDIR, "dict/dictionary.txt")
    conda:
        "envs/Picard.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice picard \
        CreateSequenceDictionary \
        R={input} \
        O={output.dict} \
        2> {log}
        """

rule FastaIndex:
# Creates fasta index file via SAMTOOLS
# NOTE: there may be some conflict between this tool and Picard's
# "CreateSequenceDictionary". As above, remove any ".fai" files
# erroneously produced besides the intended file.
    input:
        GENOME
    output:
        faidx = FAIDX,
        done = touch(join(REFDIR, "faidx.done"))
    log:
        join(LOGDIR, "faidx/faidx.stderr")
    benchmark:
        join(BENCHDIR, "faidx/faidx.txt")
    conda:
        "envs/samtools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice samtools faidx \
        {input} \
        > {output.faidx} \
        2> {log} \
        """

#rule UpdateVcfSequenceDictonary
# Updates a VCF's sequence dictionary (such as the DBSNP) with the
# new genome sequence dictionary, if not already done so.
# NOTE: the necessity of this step may depend on your chosen reference,
# known databases, and their compatibility. Software bundles, such as that
# offered by GATK, may simplify this process.
#    input:
#        dbsnp = OldDBSNP,
#        dictionary = DICTIONARY
#    output:
#        UpdatedDBSNP
#    log:
#        join(LOGDIR, "UpdatedDBSNP/UpdatedDBSNP.stderr")
#    benchmark:
#        join(BENCHDIR, "UpdatedDBSNP/UpdatedDBSNP.txt")
#    conda:
#        "envs/Picard.yaml"
#    singularity:
#        "shub://sschmeier/simg-rnaseqvariants"
#    shell:
#        """
#        nice picard \
#        UpdateVcfSequenceDictionary \
#        I={input.dbsnp} \
#        SD={input.dictionary} \
#        O={output} \
#        2> {log}
#        """

#rule BgzDBSNP
# Compresses the dbSNP in a format that is compatible with TABIX.
#    input:
#        UpdatedDBSNP
#    output:
#        DBSNP
#    log:
#        join(LOGDIR, "BgzDBSNP/BgzDBSNP.stderr")
#    benchmark:
#        join(BENCHDIR, "BgzDBSNP/BgzDBSNP.txt")
#    conda:
#        "envs/tabix.yaml"
#    singularity:
#        "shub://sschmeier/simg-rnaseqvariants"
#    shell:
#        """
#        nice bgzip \
#        {input} \
#        2> {log}
#        """

rule TabixDBSNP:
# Index for known SNPs, produced via TABIX.
# Tabix struggles with gzipped files and requires using bgzip (see above).
    input:
        DBSNP
    output:
        snpidx = SNPIDX,
        done = touch(join(REFDIR, "snpidx.done"))
    log:
        join(LOGDIR, "tabix/dbsnp/dbsnp.stderr")
    benchmark:
        join(BENCHDIR, "tabix/dsnp.txt")
    conda:
        "envs/tabix.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice tabix \
        -p vcf \
        {input} \
        2> {log}
        """

#### Read Alignment ####
# The following rules produce BAM files, sorted by coordinate, for variant
# calling. STAR's "Two-pass mode BASIC" may be used to simplify the
# pipeline but some information may be lost without producing a list of
# splice junctions for all samples. Additional information contained
# within the samples' fastq data for unmapped reads is merged with the BAM
# files, before the samples' read groups are fixed for downstream
# compatibility.

rule FastqToBam:
# Produce BAM files for unaligned FASTQ reads via PICARD.
# FastqToBam requires specifically formatted fastq files for use.
# Using the STAR option "--outSAMunmapped Within" may serve the same
# purpose but is not used for the GATK best practises. Use at your
# discretion.
#    input:
#        r1 = join(SAMPLEDIR, "{SAMPLE}_1.fastq.gz"),
#        r2 = join(SAMPLEDIR, "{SAMPLE}_2.fastq.gz")
#    output:
#        temp(join(BAMDIR, "Unmapped/{SAMPLE}.bam"))
#    log:
#        join(LOGDIR, "UnmappedBam/{SAMPLE}.stderr")
#    benchmark:
#        join(BENCHDIR, "UnmappedBam/{SAMPLE}.txt")
#    conda:
#        "envs/Picard.yaml"
#    singularity:
#        "shub://sschmeier/simg-rnaseqvariants"
#    params:
#        sample = "{SAMPLE}"
#    shell:
#        """
#        nice picard \
#        FastqToSam \
#        F1={input.r1} \
#        F2={input.r2} \
#        SM={params.sample} \
#        O={output} \
#        2> {log}
#        """

rule Star1Pass:
# Map RNA-seq reads to reference genome, primarily to identify splice
# junctions. No SAM/BAM output is required at this stage, but may help
# to generate some meaningful statistics.
    input: 
        r1 = join(SAMPLEDIR, "{SAMPLE}_1.fastq.gz"),
        r2 = join(SAMPLEDIR, "{SAMPLE}_2.fastq.gz"),
        idx = join(GENOMEIDXDIR, "INDEX.DONE")
    output:
        sj = join(STARONE, "{SAMPLE}SJ.out.tab"),
        done = touch(join(STARONE, "{SAMPLE}.Star1Pass.done"))
    log:
        join(LOGDIR, "Star1PassLog/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Star1PassBenchmark/{SAMPLE}.txt")
    conda:
        "envs/star.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads:
        8
    params:
        overhang = SJDBOVERHANG,
        prefix = join(STARONE, "{SAMPLE}")
    shell:
        """
        nice STAR \
        --runThreadN {threads} \
        --genomeDir {GENOMEIDXDIR} \
        --sjdbGTFfile {GTF} \
        --sjdbOverhang {params.overhang} \
        --readFilesIn {input.r1} {input.r2} \
        --readFilesCommand zcat \
        --outSAMtype BAM SortedByCoordinate \
        --outSAMunmapped Within \
        --outSAMattributes All \
        --outSAMstrandField intronMotif \
        --outFileNamePrefix {params.prefix} \
        2> {log}
        """

rule SpliceJunctions:
    input:
        expand(join(STARONE, "{sample}SJ.out.tab"), sample=SAMPLES)
    output:
        sj = join(BAMDIR, "SJ.all.chr.all.samples")
    log:
        join(LOGDIR, "splice_junctions/stderr.txt")
    benchmark:
        join(BENCHDIR, "splice_junctions/benchmark.txt")
    params:
        join(SCRIPTS, "awk-sjCollapseSamples.awk.txt")
    shell:
        """
        awk -f {params} \
        {STARONE}/*SJ.out.tab \
        | sort -k 1,1V -k2,2n -k3,3n \
        > {output} \
        2> {log}
        """

rule Star2Pass:
# Improved RNA-seq mapping reads to reference genome, using a list of
# splice junctions produced during the Star1Pass rule. The BAM files
# produced are sorted by coordinate for downstream compatibility.
# Refer to "FastqToBam" rule above regarding unmapped read data.
# "--outSAMstrandField" may not be required for stranded RNA-seq data.
    input: 
        r1 = join(SAMPLEDIR, "{SAMPLE}_1.fastq.gz"),
        r2 = join(SAMPLEDIR, "{SAMPLE}_2.fastq.gz"),
        sj = join(BAMDIR, "SJ.all.chr.all.samples"),
        starone = join(STARONE, "{SAMPLE}.Star1Pass.done")
    output:
        temp(join(STARTWO, "{SAMPLE}Aligned.sortedByCoord.out.bam")),
        join(STARTWO, "{SAMPLE}Log.final.out")
    log:
        join(LOGDIR, "Star2Pass/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Star2Pass/{SAMPLE}.txt")
    conda:
        "envs/star.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads:
        8
    params:
        overhang = SJDBOVERHANG,
        prefix = join(STARTWO, "{SAMPLE}")
    shell:
        """
        nice STAR \
        --runThreadN {threads} \
        --genomeDir {GENOMEIDXDIR} \
        --sjdbGTFfile {GTF} \
        --sjdbOverhang {params.overhang} \
        --sjdbFileChrStartEnd {input.sj} \
        --readFilesIn {input.r1} {input.r2} \
        --readFilesCommand zcat \
        --limitSjdbInsertNsj 2000000 \
        --outSAMtype BAM SortedByCoordinate \
        --outSAMunmapped Within \
        --outSAMattributes All \
        --outSAMstrandField intronMotif \
        --outFileNamePrefix {params.prefix} \
        2> {log}
        """

#rule MergeBamAlignment:
# Merge the Star2Pass mapped reads and information contained within the
# FastqToBam unmapped reads to retain the unmapped metadata. This rule
# is dependant upon the abpve rule "FastqToBam". Again, use at your
# discretion. Achieved via PICARD
#    input:
#        mapped = join(STARTWO, "{SAMPLE}Aligned.sortedByCoord.out.bam"),
#        unmapped = join(BAMDIR, "Unmapped/{SAMPLE}.bam")
#    output:
#        temp(join(BAMDIR, "MergedBamAlignment/{SAMPLE}.bam"))
#    log:
#        join(LOGDIR, "MergedBamAlignment/{SAMPLE}.stderr")
#    benchmark:
#        join(BENCHDIR, "MergedBamAlignment/{SAMPLE}.txt")
#    conda:
#        "envs/Picard.yaml"
#    singularity:
#        "shub://sschmeier/simg-rnaseqvariants"
#    shell:
#        """
#        nice picard \
#        MergeBamAlignment \
#        ALIGNED={input.mapped} \
#        UNMAPPED={input.unmapped} \
#        R={GENOME} \
#        O={output} \
#        2> {log}
#        """

rule FixReadGroups:
# Adds read group information to BAM files via PICARD.
# If "no files or directory" errors occur, try deactivating and
# reactivating the "base" conda enviornment.
    input:
        join(STARTWO, "{SAMPLE}Aligned.sortedByCoord.out.bam")
    output:
        protected(join(BAMDIR, "AddReadGroups/{SAMPLE}.bam"))
    log:
        join(LOGDIR, "FixReadGroups/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "FixReadGroups/{SAMPLE}.txt")
    conda:
        "envs/Picard.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    params:
        sample = "{SAMPLE}"
    shell:
        """
        nice picard \
        AddOrReplaceReadGroups \
        RGLB=lib1 \
        RGPL=illumina \
        RGPU={params.sample} \
        RGSM={params.sample} \
        I={input} \
        O={output} \
        2> {log}
        """

#### Opossum Pre-processing ####
# The following rules pre-process the BAM files for RNA-seq variant
# calling via the "Opossum" tool, which is a simple alternative to the
# GATK best practises. By combining these approaches, we hope to avoid
# introducing false positive/negative variant calls (known to occur via
# GATK's methods, and so possible for Opossum).

rule Opossum:
# Pre-process RNA-seq reads via Opossum
    input:
        join(BAMDIR, "AddReadGroups/{SAMPLE}.bam")
    output:
        protected(join(PREDIR, "Opossum/{SAMPLE}.bam"))
    log:
        join(LOGDIR, "Opossum/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/{SAMPLE}.txt")
    conda:
        "envs/Opossum.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants2"
    shell:
        """
        nice opossum \
        --BamFile={input} \
        --OutFile={output} \
        2> {log}
        """

#### GATK Pre-processing #####
# As above, the following rules follow the GATK's best practises for
# RNA-seq variant calling pipelines. These methods partly rely on 
# "hard filters" and are known to introduce false positive and negative
# results.

rule MarkDup:
# Mark duplicates for downstream analyses via PICARD.
    input:
        join(BAMDIR, "AddReadGroups/{SAMPLE}.bam")
    output:
        bam = temp(join(PREDIR, "GATK/02MarkDup/{SAMPLE}.bam")),
        metrics = join(PREDIR, "GATK/02MarkDup/{SAMPLE}.metrics")
    log:
        join(LOGDIR, "GATK/02MarkDup/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/02MarkDup/{SAMPLE}.txt")
    conda:
        "envs/Picard.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice picard \
        MarkDuplicates \
        I={input} \
        O={output.bam} \
        M={output.metrics} \
        2> {log}
        """

rule IndexBam:
# Index BAM files via SAMBAMBA.
    input:
        join(PREDIR, "GATK/02MarkDup/{SAMPLE}.bam")
    output:
        join(PREDIR, "GATK/02MarkDup/{SAMPLE}.bai")
    log:
        join(LOGDIR, "GATK/bai/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/bai/{SAMPLE}.txt")
    conda:
        "envs/sambamba.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads: 3
    shell:
        """
        nice sambamba index \
        --nthreads {threads} \
        {input} \
        {output} \
        2> {log}
        """

rule HardClip:
# Splits RNA reads into exon segments to reduce false positives.
# While GATK3 is no longer supported by the Broad Institute,
# SplitNCigarReads is not validated with GATK4.
    input:
        bam = join(PREDIR, "GATK/02MarkDup/{SAMPLE}.bam"),
        bai = join(PREDIR, "GATK/02MarkDup/{SAMPLE}.bai"),
        genome = GENOME,
        faidx = FAIDX
    output:
        temp(join(PREDIR, "GATK/03HardClip/{SAMPLE}.bam"))
    log:
        join(LOGDIR, "GATK/03HardClip/{SAMPLE}.sterr")
    benchmark:
        join(BENCHDIR, "GATK/03HardClip/{SAMPLE}.txt")
    conda:
        "envs/gatk3.yaml"
    singularity:
        "shub://sschmeier/container-gatk3"
    shell:
        """
        nice gatk3 \
        -Xmx4g \
        -T SplitNCigarReads \
        -R {input.genome} \
        -I {input.bam} \
        -o {output} \
        -rf ReassignOneMappingQuality \
        -RMQF 255 \
        -RMQT 60 \
        -U ALLOW_N_CIGAR_READS \
        2> {log}
        """

rule BaseQualityScoreRecalibration:
# Recalibrate quality scores via GATK. This is considered
# (along with indel realignment) optional with marginal improvements.
    input:
        bam = join(PREDIR, "GATK/03HardClip/{SAMPLE}.bam"),
        snpidx = SNPIDX
    output:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/recal_data.table")
    log:
        join(LOGDIR, "GATK/04BQSR/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/04BQSR/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        BaseRecalibrator \
        -R {GENOME} \
        -I {input.bam} \
        --known-sites {DBSNP} \
        -O {output} \
        2> {log}
        """

rule ApplyBQSR:
# Apply base recalibration table produced above to the BAM files
# via GATK
    input:
        genome = GENOME,
        bam = join(PREDIR, "GATK/03HardClip/{SAMPLE}.bam"),
        bqsr = join(PREDIR, "GATK/04BQSR/{SAMPLE}/recal_data.table")
    output:
        protected(join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam"))
    log:
        join(LOGDIR, "GATK/04ApplyBQSR/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/04ApplyBQSR/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        ApplyBQSR \
        -R {input.genome} \
        -I {input.bam} \
        --bqsr-recal-file {input.bqsr} \
        -O {output} \
        2> {log}
        """

##### VARIANT CALLING #####
# The following rules use various variant calling tools to produce Variant
# Call Format (VCF) files from the above processed BAM files. Various tools
# are used given their various strengths and weaknesses, so that a final
# VCF can be merged together from all tools and increase our confidence
# in those calls being true. Additionally, we call variants on both the
# GATK- and Opossum-preprocessed BAM files, permitting us to compare these
# two methods in our final analysis.

rule GATKHaplotypeCaller:
# Generate VCF from GATK-preprocessed BAM files via GATK's HAPLOTYPE CALLER
    input:
        bam = join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam"),
        dict = DICTIONARY
    output:
        temp(join(VARDIR, "GATK/HaplotypeCaller/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "GATK/HaplotypeCaller/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/HaplotypeCaller/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        HaplotypeCaller \
        -R {GENOME} \
        -I {input.bam} \
        --dbsnp {DBSNP} \
        --dont-use-soft-clipped-bases \
        --stand-call-conf 20.0 \
        -O {output} \
        2> {log}
        """

rule OpossumHaplotypeCaller:
# Generate VCF from Opossum-preprocessed BAM files via GATK's HAPLOTYPE
# CALLER
    input:
        bam = join(PREDIR, "Opossum/{SAMPLE}.bam"),
        dict = DICTIONARY
    output:
        temp(join(VARDIR, "Opossum/HaplotypeCaller/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "Opossum/HaplotypeCaller/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/HaplotypeCaller/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        HaplotypeCaller \
        -R {GENOME} \
        -I {input.bam} \
        --dbsnp {DBSNP} \
        --dont-use-soft-clipped-bases \
        --stand-call-conf 20.0 \
        -O {output} \
        2> {log}
        """

rule GATKFreebayes:
# Generate VCF from GATK-preprocessed BAM files via FREEBAYES
# Preliminary analyses suggests that Freebayes as a highly sensitive
# variant caller, and likely contains many false positives.
    input:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam")
    output:
        temp(join(VARDIR, "GATK/Freebayes/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "GATK/Freebayes/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/Freebayes/{SAMPLE}.txt")
    conda:
        "envs/freebayes.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice freebayes \
        -f {GENOME} \
        {input} \
        > {output} \
        2> {log}
        """

rule OpossumFreebayes:
# Generate VCF from Opossum-preprocessed BAM files via FREEBAYES
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        temp(join(VARDIR, "Opossum/Freebayes/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "Opossum/Freebayes/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/Freebayes/{SAMPLE}.txt")
    conda:
        "envs/freebayes.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice freebayes \
        -f {GENOME} \
        {input} \
        > {output} \
        2> {log}
        """

rule Platypus:
# Generate VCF from Opossum-preprocessed BAM files via PLATYPUS.
# Using the tools default settings appears to be incompatible with
# standard GATK pre-processing. Studies have used this tool with GATK
# previously, and so this should be invesitgated further to acquire more
# data.
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        temp(join(VARDIR, "Opossum/Platypus/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "Opossum/Platypus/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/Platypus/{SAMPLE}.txt")
    conda:
        "envs/Platypus.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants2"
    shell:
        """
        nice platypus \
        callVariants \
        --bamFiles={input} \
        --refFile={GENOME} \
        --output={output} \
        2> {log}
        """

rule GATKMutect2:
# Generate VCF from GATK-preprocessed BAM files via MUTECT2.
# Mutect2 has been demonstrated as being a very efficient variant
# caller with high sensitivity and specificity than other tools,
# when an ensemble method was not employed.
    input:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam")
    output:
        temp(join(VARDIR, "GATK/Mutect2/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "GATK/Mutect2/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/Mutect2/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        Mutect2 \
        -R {GENOME} \
        -I {input} \
        --germline-resource {GNOMAD} \
        --dont-use-soft-clipped-bases \
        -O {output} \
        2> {log}
        """

rule OpossumMutect2:
# Generate VCF from GATK-preprocessed BAM files via MUTECT2
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        temp(join(VARDIR, "Opossum/Mutect2/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "Opossum/Mutect2/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/Mutect2/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        Mutect2 \
        -R {GENOME} \
        -I {input} \
        --germline-resource {GNOMAD} \
        --dont-use-soft-clipped-bases \
        -O {output} \
        2> {log}
        """

##### END OF VARIANT CALLING #####
# The following rules consolidate all VCF files into one single VCF file
# for downstream filtering of variant calls.

rule SortPlatypus:
# Sort Platypus Variant Calls based on Genome Dictionary for compatibility.
    input:
        join(VARDIR, "Opossum/Platypus/{SAMPLE}.vcf")
    output:
        temp(join(VARDIR, "Opossum/Platypus/Sorted/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "Opossum/Platypus/Sorted/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/Platypus/Sorted/{SAMPLE}.txt")
    conda:
        "envs/Picard.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice picard \
        SortVcf \
        I={input} \
        O={output} \
        SEQUENCE_DICTIONARY={DICTIONARY} \
        2> {log}
        """

rule CombineVCF:
# Combine a sample's various VCF files via GATK's "COMBINE VARIANTS".
# The VCF annotation "set=Intersection" is applied to variants found in
# all VCF files, possibly being the most confident set of results
# obtained. Again, GATK3 is not longer supprted by the Boad Institute, but
# GATK4 does not appear to have an alternative tool for "CombineVariants".
    input:
        gatkhc = join(VARDIR, "GATK/HaplotypeCaller/{SAMPLE}.vcf"),
        ophc = join(VARDIR, "Opossum/HaplotypeCaller/{SAMPLE}.vcf"),
        gatkfb = join(VARDIR, "GATK/Freebayes/{SAMPLE}.vcf"),
        opfb = join(VARDIR, "Opossum/Freebayes/{SAMPLE}.vcf"),
        platyus = join(VARDIR, "Opossum/Platypus/Sorted/{SAMPLE}.vcf"),
        gatkmt2 = join(VARDIR, "GATK/Mutect2/{SAMPLE}.vcf"),
        opmt2 = join(VARDIR, "Opossum/Mutect2/{SAMPLE}.vcf")
    output:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf")
    log:
        join(LOGDIR, "CombineVariants/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "CombineVariants/{SAMPLE}.txt")
    conda:
        "envs/gatk3.yaml"
    singularity:
        "shub://sschmeier/container-gatk3"
    shell:
        """
        nice gatk3 \
        -T CombineVariants \
        -R {GENOME} \
        --variant:gatkhc {input.gatkhc} \
        --variant:ophc {input.ophc} \
        --variant:gatkfb {input.gatkfb} \
        --variant:opfb {input.opfb} \
        --variant:oppt {input.platyus} \
        --variant:gatkmt2 {input.gatkmt2} \
        --variant:opmt2 {input.opmt2} \
        -o {output} \
        -genotypeMergeOptions UNIQUIFY \
        2> {log}
        """

rule BgzipCombineVariants:
# Block gzip's VCF files for smaller file sizes.
    input:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf")
    output:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf.gz")
    log:
        join(LOGDIR, "BgzCombineVariants/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "BgzCombineVariants/{SAMPLE}.txt")
    conda:
        "envs/tabix.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice bgzip \
        {input} \
        2> {log}
        """

rule TabixCombineVariants:
# Block gzip's VCF files for smaller file sizes.
    input:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf.gz")
    output:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf.gz.tbi")
    log:
        join(LOGDIR, "TabixCombineVariants/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "TabixCombineVariants/{SAMPLE}.txt")
    conda:
        "envs/tabix.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice tabix \
        -p vcf \
        {input} \
        2> {log}
        """

#### VARIANT FILTRATION ####
# The following rules aim to improve both sensitivity and specificity
# by removing problematic variant calling reads using recommended best
# practise filters. We then aim to improve our confidence of variant
# calls by filtering out calls not found by all variant callers using
# both preprocessing methods.

rule rawSNP:
# Select for SNP variants identified during variant calling, via GATK's
# "SelectVariants". Can use block gzipped VCF files.
    input:
         vcf = join(VARDIR, "CombineVariants/{SAMPLE}.vcf.gz"),
         tbi = join(VARDIR, "CombineVariants/{SAMPLE}.vcf.gz.tbi")
    output:
        temp(join(FILDIR, "01rawSNP/{SAMPLE}.vcf.gz"))
    log:
        join(LOGDIR, "rawSnp/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "rawSnp/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        SelectVariants \
        -R {GENOME} \
        -V {input.vcf} \
        -select-type-to-include SNP \
        -O {output} \
        2> {log}
        """

rule AddSnpFilters:
# Adds the GATK best practises recommended "hard filters" for SNPs to the
# Above VCF file. Some true positives may be lost.
# Can use block gzipped VCF files.
    input:
        join(FILDIR, "01rawSNP/{SAMPLE}.vcf.gz")
    output:
        temp(join(FILDIR, "02AddSNPfilters/{SAMPLE}.vcf.gz"))
    log:
        join(LOGDIR, "AddSNPfilters/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AddSNPfilters/{SAMPLE}.txt")
    conda:
        "envs/gatk4.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice gatk \
        VariantFiltration \
        -R {GENOME} \
        -V {input} \
        -window 35 \
        -cluster 3 \
        --filter-name "FS" \
        --filter "FS > 30.0" \
        --filter-name "QD" \
        --filter "QD < 2.0" \
        -O {output} \
        2> {log}
        """

#### POST ANALYSIS ####
# The following rules begin to parse more meaningful data from our VCF
# files. We begin by adding annotations to the VCF files based on what is
# known of the GRCh38 genome. We then add further annotations based on
# known databases for known germline variants (DBSNP) as well as known
# somatic mutations (COSMIC).
# We then actively filter out variants with the filter tags added above
# to remove false positive calls. The VCFs are then split into moderate
# and high impact predictions as assigned by the SNPEFF tool, as well as
# alls made by variant caller consensus. These results are then compiled
# using a customer script for easier comparison using software such as
# Windows Excel.

rule AnnotateSNP:
# Annotate the SNP variants via SNPEFF using known databases for
# GRCh38. Can use black gzipped VCF files but does not subsequently
# block gzip.
    input:
        join(FILDIR, "02AddSNPfilters/{SAMPLE}.vcf.gz")
    output:
        vcf=temp(join(FILDIR, "03AnnotateSNP/{SAMPLE}.vcf")),
        stats=join(STATDIR, "AnnotateSNP/{SAMPLE}.html"),
        csvstats=join(STATDIR, "AnnotateSNP/{SAMPLE}.csv")
    log:
        join(LOGDIR, "AnnotateSNP/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateSNP/{SAMPLE}.txt")
    conda:
        "envs/snpEff.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice snpEff \
        -Xmx6g \
        -stats {output.stats} \
        -csvstats {output.csvstats} \
        GRCh38.86 \
        {input} \
        > {output.vcf} \
        2> {log}
        """

rule AnnotateDBSNP:
# Annotate SNP variants based on known databases via SNPSIFT. The
# DBSNP may contain known germline variants and so any with such
# annotations may not be somatic variants with implications in cancer.
# Can use black gzipped VCF files but does not subsequently block gzip.
    input:
        join(FILDIR, "03AnnotateSNP/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "04AnnotateDBSNP/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateDBSNP/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateDBSNP/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {DBSNP} \
        {input} \
        > {output} \
        2> {log}
        """

rule AnnotateCOSMIC:
# Annotate SNP variants based on known databases via SNPSIFT. The
# COSMIC database may contain known somatic variants and so any with
# such annotations are more likely to be somatic variants with
# implications in cancer.
    input:
        join(FILDIR, "04AnnotateDBSNP/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "05AnnotateCOSMIC/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateCOSMIC/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateCOSMIC/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {COSMIC} \
        {input} \
        > {output} \
        2> {log}
        """

rule AnnotateGNOMAD:
# Annotate SNP variants based on known databases via SNPSIFT. The
# GNOMAD database includes information from disease-specific and
# population genetic studies.
    input:
        join(FILDIR, "05AnnotateCOSMIC/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "06AnnotateGNOMAD/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateGNOMAD/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateGNOMAD/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {GNOMAD} \
        {input} \
        > {output} \
        2> {log}
        """

rule AnnotateCLINVAR1:
# Annotate SNP variants based on known databases via SNPSIFT. The
# CLINVAR database contain information on human variants with clinical
# implications.
    input:
        join(FILDIR, "06AnnotateGNOMAD/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "07AnnotateCLINVAR1/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateCLINVAR1/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateCLINVAR1/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {CLINVAR1} \
        {input} \
        > {output} \
        2> {log}
        """

rule AnnotateCLINVAR2:
# Annotate SNP variants based on known databases via SNPSIFT. The
# CLINVAR_PAPU database compliments the above database by
# including pseudoautosomal regions, alternate loci, patch
# sequences, and unlocalized, unplaced contigs.
    input:
        join(FILDIR, "07AnnotateCLINVAR1/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "08AnnotateCLINVAR2/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateCLINVAR2/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateCLINVAR2/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {CLINVAR2} \
        {input} \
        > {output} \
        2> {log}
        """

rule AnnotateExAC:
# Annotate SNP variants based on known databases via SNPSIFT. The
# ExAC database is similar to the GNOMAD database described above.
    input:
        join(FILDIR, "08AnnotateCLINVAR2/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "09AnnotateExAC/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "AnnotateExAC/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "AnnotateExAC/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift annotate \
        {ExAC} \
        {input} \
        > {output} \
        2> {log}
        """

rule ApplySnpFilters:
# Filter SNP based on GATK annotation via SNPSIFT.
    input:
        join(FILDIR, "09AnnotateExAC/{SAMPLE}.vcf")
    output:
        temp(join(FILDIR, "10ApplySnpFilters/{SAMPLE}.vcf"))
    log:
        join(LOGDIR, "ApplySnpFilters/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "ApplySnpFilters/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift filter \
        --file {input} \
        "(FILTER has 'PASS') & \
        (set = 'Intersection')" \
        > {output} \
        2> {log}
        """

rule ApplySnpModerateFilters:
# Filter SNP based on 'Moderate' impact mutations via SNPSIFT.
    input:
        join(FILDIR, "10ApplySnpFilters/{SAMPLE}.vcf")
    output:
        join(FILDIR, "10ApplySnpFilters/Moderate/{SAMPLE}.vcf")
    log:
        join(LOGDIR, "ApplySnpModerateFilters/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "ApplySnpModerateFilters/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift filter \
        --file {input} \
        "(ANN[0].IMPACT has 'MODERATE')" \
        > {output} \
        2> {log}
        """

rule ApplySnpHighFilters:
# Filter SNP based on 'High' impact mutations via SNPSIFT.
    input:
        join(FILDIR, "10ApplySnpFilters/{SAMPLE}.vcf")
    output:
        join(FILDIR, "10ApplySnpFilters/High/{SAMPLE}.vcf")
    log:
        join(LOGDIR, "ApplySnpHighFilters/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "ApplySnpHighFilters/{SAMPLE}.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice SnpSift filter \
        --file {input} \
        "(ANN[0].IMPACT has 'HIGH')" \
        > {output} \
        2> {log}
        """

rule CompileModerateSNPvcf:
    input:
        expand(join(FILDIR, "10ApplySnpFilters/Moderate/{sample}.vcf"), sample=SAMPLES)
    output:
        join(FILDIR, "10ApplySnpFilters/Moderate/ModerateSnpTable.txt")
    log:
        join(LOGDIR, "CompileModerateSNPvcf/vcfcompile.stderr")
    params:
        extra="--snpeff"
    shell:
        """
        python scripts/vcfcompile.py \
        --ann QD \
        --warn \
        {params.extra} \
        --snpeffType MODERATE \
        {input} \
        > {output} \
        2> {log} \
        """

rule CompileHighSNPvcf:
    input:
        expand(join(FILDIR, "10ApplySnpFilters/High/{sample}.vcf"), sample=SAMPLES)
    output:
        join(FILDIR, "10ApplySnpFilters/High/HighSnpTable.txt")
    log:
        join(LOGDIR, "CompileHighSNPvcf/vcfcompile.stderr")
    params:
        extra="--snpeff"
    shell:
        """
        python scripts/vcfcompile.py \
        --ann QD \
        --warn \
        {params.extra} \
        --snpeffType HIGH \
        {input} \
        > {output} \
        2> {log} \
        """

rule bcftools_stats:
# Produce statistical data using BCFTools.
    input:
        join(VARDIR, "CombineVariants/{SAMPLE}.vcf")
    output:
        join(STATDIR, "bcfstat/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "bcfstat/{SAMPLE}.stderr")
    benchmark:
        join(STATDIR, "bcfstat/{SAMPLE}.txt")
    conda:
        "envs/bcftools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice bcftools stats \
        --fasta-ref {GENOME} \
        --samples - {input} \
        > {output} \
        2> {log}
        """

rule opossum_idxstats:
# Prints stats of index files corresponding to input file.
# Requires indexed BAM files.
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        join(STATDIR, "Opossum/idxstats/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "Opossum/idxstats/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/idxstats/{SAMPLE}.txt")
    conda:
        "envs/samtools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice samtools idxstats \
        {input} \
        > {output} \
        2> {log}
        """

rule gatk_idxstats:
# Prints stats of index files corresponding to input file.
# Requires indexed BAM files.
    input:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam")
    output:
        join(STATDIR, "GATK/idxstats/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "GATK/idxstats/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/idxstats/{SAMPLE}.txt")
    conda:
        "envs/samtools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice samtools idxstats \
        {input} \
        > {output} \
        2> {log}
        """

rule opossum_samstats:
# Generate statistics from BAM files via SAMTOOLS
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        join(STATDIR, "Opossum/samstats/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "Opossum/samstats/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/samstats/{SAMPLE}.txt")
    conda:
        "envs/samtools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice samtools stats \
        {input} \
        > {output} \
        2> {log}
        """

rule gatk_samstats:
# Generate statistics from BAM files via SAMTOOLS.
    input:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam")
    output:
        join(STATDIR, "GATK/samstats/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "GATK/samstats/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/samstats/{SAMPLE}.txt")
    conda:
        "envs/samtools.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice samtools stats \
        {input} \
        > {output} \
        2> {log}
        """

rule opossum_flagstat:
# Generate flagged statistics for BAM files via SAMBAMBA.
    input:
        join(PREDIR, "Opossum/{SAMPLE}.bam")
    output:
        join(STATDIR, "Opossum/flagstat/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "Opossum/flagstat/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "Opossum/flagstat/{SAMPLE}.txt")
    conda:
        "envs/sambamba.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads: 3
    shell:
        """
        nice sambamba flagstat \
        --nthreads {threads} \
        {input} \
        > {output} \
        2> {log}
        """

rule gatk_flagstat:
# Generate flagged statistics for BAM files via SAMBAMBA.
    input:
        join(PREDIR, "GATK/04BQSR/{SAMPLE}/{SAMPLE}.recal.bam")
    output:
        join(STATDIR, "GATK/flagstat/{SAMPLE}.stdout")
    log:
        join(LOGDIR, "GATK/flagstat/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "GATK/flagstat/{SAMPLE}.txt")
    conda:
        "envs/sambamba.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    threads: 3
    shell:
        """
        nice sambamba flagstat \
        --nthreads {threads} \
        {input} \
        > {output} \
        2> {log}
        """

rule multiqc_samplenames:
    input:
        SAMPLESHEET
    output:
        join(RESULTS, "multiqc-samplenames.txt")
    shell:
        """
        echo -e "sampleid\tsamplename" > {output}; \
        cat {input} | cut -f 1,{MULTIQC_COL} >> {output}
        """

rule Multiqc:
# -o = output directory.
    input:
        join(RESULTS, "multiqc-samplenames.txt"),
        expand(join(STARTWO, "{sample}Log.final.out"), sample=SAMPLES),
        expand(join(PREDIR, "GATK/04BQSR/{sample}/recal_data.table"), sample=SAMPLES),
        expand(join(STATDIR, "AnnotateSNP/{sample}.csv"), sample=SAMPLES),
        expand(join(STATDIR, "bcfstat/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "Opossum/idxstats/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "GATK/idxstats/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "Opossum/samstats/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "GATK/samstats/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "Opossum/flagstat/{sample}.stdout"), sample=SAMPLES),
        expand(join(STATDIR, "GATK/flagstat/{sample}.stdout"), sample=SAMPLES)
    output:
        directory(REPORTS)
    log:
        join(LOGDIR, "multiqc/multiqc.stderr")
    benchmark:
        join(BENCHDIR, "multiqc/multiqc.txt")
    conda:
        "envs/multiqc.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice multiqc \
        --sample-names {input[0]} \
        -o {REPORTS} \
        {STARTWO} \
        {PREDIR} \
        {STATDIR} \
        2> {log}
        """

rule highVCFsnpExtractFields:
    input:
        expand(join(FILDIR, "10ApplySnpFilters/High/{sample}.vcf"), sample=SAMPLES)
    output:
        join(FILDIR, "10ApplySnpFilters/High/unique_genes.txt")
    log:
        join(LOGDIR, "VCFsnpExtractFields/VCFsnpExtractFields.stderr")
    benchmark:
        join(BENCHDIR, "VCFsnpExtractFields/VCFsnpExtractFields.txt")
    params:
        join(SCRIPTS, "selectHigh.awk.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        cat {input} \
        | perl scripts/vcfEffOnePerLine.pl \
        | SnpSift extractFields - \
        CHROM \
        POS \
        "ANN[*].IMPACT" \
        "ANN[*].GENE" \
        | awk -f {params} \
        | cut -f 4 | sort | uniq \
        > {output}
        """

rule moderateVCFsnpExtractFields:
    input:
        expand(join(FILDIR, "10ApplySnpFilters/Moderate/{sample}.vcf"), sample=SAMPLES)
    output:
        join(FILDIR, "10ApplySnpFilters/Moderate/unique_genes.txt")
    log:
        join(LOGDIR, "VCFsnpExtractFields/VCFsnpExtractFields.stderr")
    benchmark:
        join(BENCHDIR, "VCFsnpExtractFields/VCFsnpExtractFields.txt")
    params:
        join(SCRIPTS, "selectHigh.awk.txt")
    conda:
        "envs/SnpSift.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        cat {input} \
        | perl scripts/vcfEffOnePerLine.pl \
        | SnpSift extractFields - \
        CHROM \
        POS \
        "ANN[*].IMPACT" \
        "ANN[*].GENE" \
        | awk -f {params} \
        | cut -f 4 | sort | uniq \
        > {output}
        """

rule BgzipModerateVariants:
# Block gzip's Moderate impact VCF files for smaller file sizes.
    input:
        vcf = join(FILDIR, "10ApplySnpFilters/Moderate/{SAMPLE}.vcf"),
        genes = join(FILDIR, "10ApplySnpFilters/Moderate/unique_genes.txt"),
        table = join(FILDIR, "10ApplySnpFilters/Moderate/ModerateSnpTable.txt")
    output:
        join(FILDIR, "10ApplySnpFilters/Moderate/{SAMPLE}.vcf.gz")
    log:
        join(LOGDIR, "BgzModerateVCF/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "BgzModerateVCF/{SAMPLE}.txt")
    conda:
        "envs/tabix.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice bgzip \
        {input.vcf} \
        2> {log}
        """

rule BgzipHighVariants:
# Block gzip's High impact VCF files for smaller file sizes.
    input:
        vcf = join(FILDIR, "10ApplySnpFilters/High/{SAMPLE}.vcf"),
        genes = join(FILDIR, "10ApplySnpFilters/High/unique_genes.txt"),
        table = join(FILDIR, "10ApplySnpFilters/High/HighSnpTable.txt")
    output:
        join(FILDIR, "10ApplySnpFilters/High/{SAMPLE}.vcf.gz")
    log:
        join(LOGDIR, "BgzHighVCF/{SAMPLE}.stderr")
    benchmark:
        join(BENCHDIR, "BgzHighVCF/{SAMPLE}.txt")
    conda:
        "envs/tabix.yaml"
    singularity:
        "shub://sschmeier/simg-rnaseqvariants"
    shell:
        """
        nice bgzip \
        {input.vcf} \
        2> {log}
        """