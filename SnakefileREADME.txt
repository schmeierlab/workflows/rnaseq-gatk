README

This file contains details regarding the use of Tom Manning's Snakemake
Workflow, designed primarily for the human genome, in order to identify
SNP and INDEL variants from a refeence genome, using RNA-seq data. With 
some tweaks, this workflow may be suitable for other organisms.

CONFIG.YML

We will first consider the config file, "config.yml", which should be
contained within the same working directory as the Snakefile used for
this pipeline. The config file contains the following information:

REFDIR:
    the directory where your genome reference, and all related files
    is contained.
GENOME:
    the file path to your reference genome (such as the human genome
    GRCh38), within REFDIR.
DICTIONARY:
    the genome dictinary file for your reference genome. This is
    produced by the rule "GenomeDictionary" in the Snakefile.
DICT_IDX:
    the file path for your genome dictionary's index. This is produced
    by the rule "IndexDictionary" in the Snakefile.
DBSNP:
    the file path for Single Nucleotide Polymorphism Database (dbSNP),
    required for several rules within in the Snakefile.
SNPIDX:
    the file path for dbSNP's index. This is produced by the rule
    "TabixDBSNP"
GTF:
    the file path for the General Transfer Format (GTF) file which
    containe the annotations to be used for the reference genome.
GENOMEIDXDIR:
    The directory where files are contained when building the index
    for your reference genome. You may wish to distinguish this
    directory in some way, such as including the overhanging reads for
    splice junctions (see below).
SJDBOVERHANG:
    This designates the "overhang" value for your indexing and mapping
    rules within the Snakefile. This is typically the length of your RNA
    reads minus 1 (eg RNA-seq read length = 125, SJDBOVERHANG = 124)
SAMPLEDIR:
    The directory where your RNA-seq samples can be found.
SAMPLESHEET:
    This directory will contain a text file containing the samples that
    you wish to process with the Snakemake workflow.
SCRIPTS:
    This directory will contain any relevant scripts for the workflow.
    This is primarily for the rule "Splice_Junctions" as part of the
    mapping of reads with the Spliced Trasncript Alignment to a Reference
    (STAR) tool.
RESULTS:
    This directory will contain all the resulting outputs from the
    workflow,     including Binary Alignment/Mapping (BAM), Variant
    Calling Format (VCF), and related files.
MULTIQC_COL:
    This will designate which column to use in order to name your samples
    from your SAMPLESHEET for the MultiQC report.

SNAKEFILE.TEXT 

The Snakefile contains the rules by which Snakemake operates, resulting
primarily in 2 types of VCF files for our RNA-seq samples: one containing
data for Single Nucleotide Polymorphisms (SNPs) and another containing
data for short inserations and deleations (INDELS). These files serve as
the input for the rule "all", which can only be achieved after working
through each of the rules found in the Snakefile. Snakemake achieves this
by using the output for one rule as a subsequent input for the next rule
until the rule "all" is achieved.

The Snakefile also contains additional directories like the confi file,
however these directories are reliant on code and so cannot be included in
the config file at this time.

To put it simply, Snakemake will:

    * Generate a Genome Index 
    * Map our RNA-seq reads to our reference (BAM files)
    * Pre-process our BAM files for RNA-seq variant calling
    * Call variants between our data and our reference genome (VCF files)
    * Filter the variants found to improve senesitivity and specificity

For more details, refer to the list below which discusses each of the
rules in more detail.

GENOME INDEXING 

Rule: StarIndex:
    This rule produces an index for our reference genome. By building
    an index before mapping, our mapping tool (STAR) can more quickly
    find nucleotide sequences which align between our reference and
    our RNA-seq data (much like using a textbook's index to find a
    relevant page, rather than reading through the entire book).

RNA-SEQ MAPPING 

Rule: Star1Pass:
    This rule begins the process of mapping our RNA-seq reads to our
    reference genome. Organisms that are capable of splicing genes
    requires the use of STAR's "two-pass" setting. After running this
    "first-pass", STAR is able to identify splice junctions which can be
    used in the subsequent rule Star2Pass to better align our reads and
    produce less less-positive variant calls.

Rule: SpliceJunctions:
    This rule uses a simple scripts to collapse the splice junctions
    found for each read of our sample into a single file for use in
    the Star2Pass rule.

Rule: Star2Pass:
    This rule functions similarly to the Star1Pass rule, with the addition
    of annotated splice junctions.

PREPROCESSING

Variant Calls were made by first processing our BAM files according to
"The GATK Best PRactises for variant calling on RNAseq", available in
the link below:

https://software.broadinstitute.org/gatk/documentation/article.php?id=3891

While the Broad Institute's DNAseq best practises were developed over
several years, these RNAseq best practises are still being developed,
meaning that false positives and negatives should not be unexpected.

Rule: FixReadGroups:
    In this workflow, we used the Genome Analysis Toolkit (GATK) for
    variant calling. These tool requires some additional information
    in our BAM files for downstream processes.
    Picard, a GATK tool, uses the function "AddOrReplaceReadGroups"
    to add read groups missing from our BAM files that the GATK tools
    require. Details on the read groups added can be found in the rules
    parameters ("params").

Rule: MarkDup:
    In this rule, the Picard tool is labelling or "marking" duplicate reads
    so that tools downstream in the workflow do not include them as false
    positives in their output.

Rules: GenomeDictionary, IndexDictionary, IndexBam:
    One RNA-seq specific tool available in GATK is "SplitNCigarReads".
    This tool requires a Genome dictionary, an index of that dictionary,
    and an index for the BAM file being processed. These rules produce
    these files via the tools PICARD, SAMTOOLS, and SAMBAMBA. SAMTOOLS
    and SAMBAMBA are separate from GATK, and both serve similar functions,
    though originally SAMBAMBA allowed for parallel processing, not
    originally availble in SAMTOOLS.

Rule: HardClip:
    This rule uses the GATK "SplitNCigarReads" function discussed
    previously. Some reads can be misaligned around splice junctions,
    resulting in false positive calls. SplintNCigarReads is able to
    recognize these misalignments, and filter or "hard clip" the
    misalignment, leaving the portion of the read correctly aligned to
    the genome.
    The quality scores assigned by the STAR tool also differ from the ones
    used by GATK. For example, the max value for STAR is 255, while GATK's
    max value is 60. SplintNCigarRead's additional function,
    ReassignOneMappingQuality adjusts quality scores to GATK-friendly
    values.

Rule: TabixDBSNP:
    This produces an index for our known SNP database (dbSNP), via the
    tool TABIX. This index is required for the following rule.

Rule: BaseQualityScoreRecalibration:
    This step is recommended by the GATK best practises, though they admit
    that the effect can be marginal when using good quality data. Another
    GATK recommended step that you may wish to include is "Indel
    Realignment", which can obtain more true positive indels, though the
    effect again is noted as being marginal.

VARIANT CALLING

Now that our BAM files have been prepared for variant calling, we can begin
to make variant calls. Be aware that as the RNA-seq preprocessing methods
above were suggested by the Broad Institute, here we will also use the
Broad Institute's Variant Calling tool 'HaplotypeCaller'. Other
preprocessing and variant calling methods are available, such as the
'Opossum' and 'Platypus' tools.

Rule: HaplotypeCaller:
    This tool makes the variant calls between our RNA-seq reads and our
    reference genome. The "-stand_call_conf" setting can be reduced from
    20.0 to increase sensitivity (at the cost of specificity).

Rule: ValidateVCF:
    This tool ensures that our VCF files are valid and of the correct
    format, which may otherwise disrupt downstream processing.

VARIANT FILTRATION

Variant filtration may be one of the most important steps with regards to
RNA-seq variant calling, and there are many nuances to be considered that
may affect sensitivitiy (being able to make a variant call) and specificity
(where a variant call is a true variant).

Each study may decide to implement their own filtering criteria, and for
this reason the Snakefile retains our 'raw' VCF files in the interests
of saving time for future runs. It is also recommended that, if one
decides to use alternative variant calling tools, the resulting raw
VCF files are contained in a separate directory detailing the variant
caller used (e.g. /HaplotypeCaller/). This may be useful for comparing
the performance of different variant callers/implementing a consensus
variant calling method.

Rules: rawX, AddXFilters, AnnotateX, AnnotateKnownX, ApplyXFilters, and
ApplyXModerate/HighFilters (where "X" can be "SNP" or "INDEL"):

    # rawX uses GATK's "SelectVariants" for either SNPs or INDELS, as
    indentified by GATK's tools. The resulting files are contained within
    their own directories (e.g. /SNP/)

    # AddXFilters adds hard filter annotations, but the filters are not
    applied at this stage. These hard filters differ between SNPs or
    INDELs, as suggested by GATK's best practises. Such hard filters 
    may remove true positives, which users should be aware of.
    
    # AnnotateX uses the SnpEff tool to predict variant functions (such as
    missing start codons, frameshifts, etc) using one of SnpEff's databases
    (such as their GRCh38.86 database, used in our current piepline).

    # AnnotateKnownX uses the SnpSift tool to annotate variants from known
    databases (such as dbSNP's "00-All.vcf" used in our current pipeline).
    
    # ApplyXFilters filters all variants that do not meet the criteria
    specified at the "AddXFilters" stage, using the SnpSift Filter tool. Due
    to issues with GATK not applying filters correctly in all instances, we again
    use the same filters but using SnpSift Filter to help filter out unwanted,
    low confidence variants.
    Our current pipeline has added two additional filtering criteria as well as
    GATK's recommendations: FILTER has 'PASS', which should remove variants
    that do not meet GATK's filtering criteria, and DP > 10.

    # ApplyXLow/Moderate/High/LOF/NMDFilters produces separate VCF files based on
    the IMPACT annotation a variant may have. These files afe contained within
    their own directories (e.g. /HIGH/). LOF and NMD stands for "loss of function"
    and "nonsense-mediated decay", offered by SnpEff.

GENERATING MORE USER FRIENDLY DATA TABLES FOR ANALYSIS

Rules: CompileXYvcf

Here, several rules use a custom python script ("vcfcompile") developed by Massey
University's Dr Sebastian Schmeier, to compile the QD values for each of the variants
obtained for each of SnpEff's impact annotations (Low, Moerate, X, etc) for both SNPs
and INDELS (Y) into a single tab-delimited text file. We chose the QD value (quality
by depth) as it is a normalized representation of the variants quality based on depth
of reads for that variant.

Each variant entry contains the following information:
    CHROM, the chromosome where that ariant was obtained
    POS, the position where that variant was obtained
    ID, the ID annotated in the AnnotateKnownX rule
    REF, the allele found in the reference genome
    ALT, the allele variant obtained
    GENES, the name of the gene at this position
    SAMPLE, the name of each sample, using the Snakemake {SAMPLE} wildcard nomenclature

GENERAL STATISTICAL DATA

Rules: bcftools_stats, samtools_idxstats, samtools_stats, sambamba_flagstat

Here we use the above tools and functions to generate some general statistics that
is then compiled using the ultiQC rules below

MUKTIQC RULES

Rules: multiqc_samplenames, MultiQC

multiqc_samplenames uses a simple linux script to produce a list of the sample names
used in the study, with MultiQC then using those samplenames when it compiles the
statistic results from the above rules. MultiQC's rule contains the various output
folders for those statistical rules in order to compile the results into a single HTML file.

GENERATING LISTS OF UNIQUE GENE NAMES FOR SENE SET ENRICHMENT ANALYSIS

Rules: VCFsnpExtractFields, VCFindelExtractFields

The above rules can be used to obtain a list of genes for whom variants were obtained during
the study, which can be used for gene set enrichment analysis, such as via Enrichr, for
further analysis. These rules use a combination of SnpSift's "extractFields" function and
some basic Linux "awk" command lines to extract the gene names contained within column 4
of the script.